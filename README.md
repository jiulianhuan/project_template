## 九次方-九连环大数据项目模板

### Project Introduction
Project info in the doc directory. 
### Technological Framework

Tech | Name | Version
-----|------|----
JDK    | Oracle JDK    | 1.8
Web Container    | Tomcat    | 8.0
MVC Framework   | Spring MVC    | 4.2.4
View TempLate    | Beetl    | 2.2.8
Authority Framework    | Shiro    | 1.2.3
DB Connection Pool    | BoneCP    | 0.8
ORM    | MyBatis    | 3.2.8
Building Tools    | maven    | 2.10
Code Manage Tools    | Git    | 2.5.4
IDE    | IDEA or Eclipse    | Latest edition




