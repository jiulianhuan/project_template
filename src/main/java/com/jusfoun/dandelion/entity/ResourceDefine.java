package com.jusfoun.dandelion.entity;

/**
 * ResourceDefine
 *
 * @author Connor
 * @version 1.0
 * @date 16/1/28 下午5:55
 **/
public class ResourceDefine {

  private Integer resourceId;

  private Integer resourceStatusId;

  private Integer nodeId;

  private String resourceName;

  private String resourceDesc;

  private Integer govId;

  private Integer collectType;

  public Integer getResourceId() {
    return resourceId;
  }

  public void setResourceId(Integer resourceId) {
    this.resourceId = resourceId;
  }

  public Integer getResourceStatusId() {
    return resourceStatusId;
  }

  public void setResourceStatusId(Integer resourceStatusId) {
    this.resourceStatusId = resourceStatusId;
  }

  public Integer getNodeId() {
    return nodeId;
  }

  public void setNodeId(Integer nodeId) {
    this.nodeId = nodeId;
  }

  public String getResourceName() {
    return resourceName;
  }

  public void setResourceName(String resourceName) {
    this.resourceName = resourceName;
  }

  public String getResourceDesc() {
    return resourceDesc;
  }

  public void setResourceDesc(String resourceDesc) {
    this.resourceDesc = resourceDesc;
  }

  public Integer getGovId() {
    return govId;
  }

  public void setGovId(Integer govId) {
    this.govId = govId;
  }

  public Integer getCollectType() {
    return collectType;
  }

  public void setCollectType(Integer collectType) {
    this.collectType = collectType;
  }

  @Override
  public String toString() {
    return "ResourceDefine{" +
      "resourceId=" + resourceId +
      ", resourceStatusId=" + resourceStatusId +
      ", nodeId=" + nodeId +
      ", resourceName='" + resourceName + '\'' +
      ", resourceDesc='" + resourceDesc + '\'' +
      ", govId=" + govId +
      ", collectType=" + collectType +
      '}';
  }
}
