package com.jusfoun.dandelion.controller;

import com.jusfoun.dandelion.entity.ResourceDefine;
import com.jusfoun.dandelion.service.ResourceDefineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import java.util.Date;
import java.util.List;

/**
 * IndexController
 *
 * @author Connor
 * @version 1.0
 * @date 16/1/9 下午4:56
 **/
@Controller
public class IndexController {

    private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    @Qualifier("ResourceDefineServiceImpl")
    private ResourceDefineService resourceDefineService;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    String home(Model model) {
//        List<ResourceDefine> map = resourceDefineService.findAllList();
//
//        for(int i=0;i<map.size();i++){
//            logger.debug(map.get(i).toString());
//        }

//        model.addAttribute("time", new Date());
//        model.addAttribute("message", "Hello Spring Boot Beetl!");
        return "index";
    }

    @RequestMapping(value = "")
    ModelAndView data(){
        ModelAndView view = new ModelAndView();
        view.setViewName("/template/index");
        return view;

    }
}