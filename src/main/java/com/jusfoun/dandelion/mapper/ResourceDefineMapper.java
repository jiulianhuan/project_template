package com.jusfoun.dandelion.mapper;

import com.jusfoun.dandelion.entity.ResourceDefine;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;


/**
 * ResourceDefineMapper
 *
 * @author Connor
 * @version 1.0
 * @date 16/1/28 下午4:03
 **/
public interface ResourceDefineMapper {

  @Select("SELECT * FROM J_RESOURCE_DEFINE")
  @Results(value = {
    @Result(property = "resourceId", column = "resource_id", javaType = Integer.class, jdbcType = JdbcType.NUMERIC),
    @Result(property = "resourceStatusId", column = "resource_status_id", javaType = Integer.class, jdbcType = JdbcType.NUMERIC),
    @Result(property = "nodeId", column = "node_id", javaType = Integer.class, jdbcType = JdbcType.NUMERIC),
    @Result(property = "resourceName", column = "resource_name", javaType = String.class, jdbcType = JdbcType.VARCHAR),
    @Result(property = "resourceDesc", column = "resource_desc", javaType = String.class, jdbcType = JdbcType.VARCHAR),
    @Result(property = "govId", column = "gov_id", javaType = Integer.class, jdbcType = JdbcType.NUMERIC),
    @Result(property = "collectType", column = "collect_type", javaType = Integer.class, jdbcType = JdbcType.NUMERIC) })
  List<ResourceDefine> findAllList();

}
