package com.jusfoun.dandelion.service;

import com.jusfoun.dandelion.entity.ResourceDefine;
import com.jusfoun.dandelion.mapper.ResourceDefineMapper;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ResourceDefineServiceImpl
 *
 * @author Connor
 * @version 1.0
 * @date 16/1/28 下午4:06
 **/

@Service("ResourceDefineServiceImpl")
public class ResourceDefineServiceImpl implements ResourceDefineService {


  @Autowired
  private SqlSessionTemplate sqlSessionTemplate;

  private ResourceDefineMapper getMapper() {
    return sqlSessionTemplate.getMapper(ResourceDefineMapper.class);
  }


  public List<ResourceDefine> findAllList() {
    return getMapper().findAllList();
  }
}
