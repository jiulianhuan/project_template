package com.jusfoun.dandelion.service;

import com.jusfoun.dandelion.entity.ResourceDefine;

import java.util.List;

/**
 * Created by elvis on 16/1/28.
 */
public interface ResourceDefineService {

  List<ResourceDefine> findAllList();

}
